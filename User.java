/**
 * Created by marku_000 on 2016-09-27.
 */
/**
 * This class represents the data for a user of the bank
 */

public class User {
    // private instance variables
    // name: a String representing the username
    // password: a String representing the password
    // account: an Account representing the account

    private String name = "";
    private String password = "";
    private int accountId = 0;
    private Account account;







    // Constructor with parameters
    // Create a new account and record the "transaction"
    public User(String name, String password, int accountId) {
        this.name = name;
        this.password = password;
        this.accountId = accountId;
        this.account = new Account(0, accountId);

    }



    /**
     * Returns the account id
     * @return id
     */

    public int getId() {
        // code here
        return this.accountId;
    }

    /**
     * Returns the username
     * @return name
     */

    public String getUsername() {

        // code here
        return this.name;
    }

    /**
     * Returns the password
     * @return password
     */
    public String getPassword() {
        // code here
        return this.password;
    }

    /**
     * Returns the user Account
     * @return account
     */

    public Account getAccount() {
        // code here
        return account;
    }
}
