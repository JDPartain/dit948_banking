/**
 * Created by jonathan on 2016-09-26.
 */
    /**
     * This class implements a user account, represented by a user id
     * (unique for each account) and an amount (or balance)
     */
public class Account {
    // private instance variables
    // id: an integer representing the account id
    // amount: the account balance
    // constructor with parameters
    private int id;
    private int amount;



    public Account(int amount, int id) {
    // code here
        this.amount = amount;
        this.id = id;
    }
        /**
         * Withdraw a given amount of money from the account and record the
         * transaction
         * @param deductAmount the amount to withdraw
         * @return true if the withdraw succeeds, false otherwise
         */
    public boolean withdraw(int deductAmount){
    // Check if deductAmount is larger than acc. balance
        if (this.getAmount() > deductAmount) {
            this.amount -= deductAmount;


            return true;
        }
        else {
            return false;
        }


    }
        /**
         * Deposit a given amount to the account and
         * record the transaction
         * @param addAmount amount to be deposited
         * @return true
         */
    public boolean deposit(int addAmount){
        // code here

        this.amount += addAmount;

        return true;
    }
        /**
         * Transfer money from this account to user B, and
         * record the transaction
         * @param B user to transfer money to
         * @param amountToTransfer the amount to transfer
         * @return true if the transfer is possible, false otherwise
         */
    public boolean transferMoney(User B,int amountToTransfer){
    // code here
        // TODO: Record transaction
        if (this.getAmount() < amountToTransfer) {
            return false;
        }
        else {

            withdraw(amountToTransfer);
            
            B.getAccount().deposit(amountToTransfer);


            return true;
        }
    }
        /**
         * Returns the balance and records the transaction
         * @return amount
         */
    public int getAmount() {
        // here code

        return this.amount;

    }
        /**
         * Returns the account id
        * @return id
         */
    public int getId(){
        // code here
        return this.id;
    }
}

