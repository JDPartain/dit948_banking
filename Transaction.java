/**
 * Created by jonathan on 2016-09-26.
 */
public class Transaction {
    /**
     * This class implements a transaction performed by the bank users
     */
     // private instance variables
    // type: a String to record the type of transaction
    // account: an Account used by the transaction
    // constructor with parameters
    private Account account;
    private String type;
    public Transaction(Account account, String type) {
    // code here
        this.account = account;
        // Deposit/Transaction/Withdraw
        this.type = type;
    }
    /**
     * Returns the Account object
     */

    public Account getAccount() {
    // code here
         return this.account;

    }
        /**
         * String representation of a transaction
         */
    public String toString() {
    // code here
        return "Type: " + this.type;

    }

}
