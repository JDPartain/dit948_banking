/**
 * Created by marku_000 on 2016-09-27.
 */


import java.util.ArrayList;
import java.util.Scanner;

/**
 * This class implements the data structures of the bank. An array of users and
 * array of transactions are used to keep track of users and transactions
 * Note: you are allowed to use ArrayLists, but you don't need to
 */

public class Bank {
    private static ArrayList<String> users = new ArrayList<>(100);
    static ArrayList<String> passwords = new ArrayList<>(100);
    private static ArrayList<User> userList = new ArrayList<>(100);
    public static ArrayList<Transaction> transactions = new ArrayList<>(100);




    private String u = "";
    private String p = "";
    private int id = 0;
    User user;

    // Declaration of class variables,
    // id: a public static integer representing a user id, initially 0
    // users: a private array of User objects
    // transactions: a public static array of Transaction objects
    // Declare your own variables, if needed
    // code here
    // default constructor
    // you can assume there won't be more than 100 users
    // and 100 transactions
    public Bank() {
        // code here
    }
    /**
     * Checks whether a user is present in the User array, given the username
     * and the password
     *
     * @param u  username
     * @param p  password
     * @return true or false, accordingly
     */

    public boolean findUserByUsernamePwd(String u, String p) {
        Boolean findUserByUsernamePwdbool = false;
        this.u = u;
        for (int i = 0; i <= users.size() -1; i++){
            if (users.get(i).equals(u) && passwords.get(i).equals(p)){
                findUserByUsernamePwdbool = true;
            }
        }
        // code here
        return findUserByUsernamePwdbool;
    }
    int tmpID = 0;
    public boolean checkLogin(String u, String p){
        this.u = u;
        this.p = p;
        for (int i = 0; i <= users.size() -1; i++){
            if (users.get(i).equals(u)){
                tmpID = i;
            }
        }
        if (passwords.get(tmpID).equals(p)){
            return true;
        }
        else{
            return false;
        }
    }

    /**
     * String representation of the list of users of the
     * online bank
     */
    @Override
    public String toString() {
        int StringCounter = 0;
        String out = "";
        // code here
        int i = 0;
        for(String u : users) {
            out += "User " + i + ": " + u + "\n";
            i++;
        }
        return out;
    }
    /**
     * Returns the User associated with a username and a password
     *
     * @param u  username
     * @param p  password
     * @return a User object or null
     */

 public User getUserFromUsrPwd(String u, String p) {
 // code here
	 int a = 0;
     for (User s : userList) {
         if (s.getUsername().equals(u) && s.getPassword().equals(p)) {
             return userList.get(a);
         }
         a++;
     }


	 return null;
 }
 /**
 * Returns the User object associated with a given username
 *
 * @param u username
 * @return the User with username u, if present in the array of
 * users; null otherwise
 */

 public User getUserByUsr(String u) {
 // code here
     int a = 0;
     for(User s : userList) {

         if (s.getUsername().equals(u)){
             return userList.get(a);
         }
         a++;
     }
     return null;
 }

    /**
     * Prompt the user to enter a username and a password from the console. Then
     * use the chosen username and password to create a new user and add it to
     * the array of users. Hint: Each user must be assigned a new id.
     */
    int UserCounter = 0;
    String tmpUserName;
    Boolean CheckUserName = false;
    public void addUser() {
        Scanner scan = new Scanner(System.in);
        CheckUserName = true;
        Boolean toggle = true;

        do {
            System.out.println("Write your username");
            tmpUserName = scan.next();
            if(users.contains(tmpUserName)) {
                System.out.println("Username already in use");
                toggle = true;
            }
            else {
                users.add(tmpUserName);
                // System.out.println("Username recorded " + tmpUserName);
                System.out.println("Write your password");
                String pass = scan.next();
                passwords.add(pass);
                userList.add(new User(tmpUserName, pass, id));
                System.out.println("New user created");
                id++;

                toggle = false;
            }
        }while (toggle);
        /**  Users[UserCounter] = tmpUserName;
         System.out.println("Write your password");
         Passwords[UserCounter] = scan.next();
         UserCounter++;*/
        // code here
    }
}