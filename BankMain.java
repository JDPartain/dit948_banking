import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.Scanner;
/**
 * Created by jonathan on 2016-09-26.
 */
public class BankMain {
    /**
     * Class representing a online banking application for the second assignment of
     * DIT948, 2016 edition. This is the main class for the application, interacting
     * with the user, creating accounts and performing various transactions More
     * information about the interface can be found in Testcases.txt
     */
    public static void main(String[] args) {
        System.out.println("Welcome to the DIT948 online bank");
        MainMenu();
        //goes to the main menu directly after a welcome message
    }
    public static void MainMenu(){
        Bank bank = new Bank();
        while (true) {
            Scanner scanner = new Scanner(System.in);
            // Implement here the interaction between the user and the
            // online banking system.
            // As described in the Assignment text, the interface contains a Main
            // menu, and a SubMenu. The program should return to the Main Menu
            // whenever the user exits the SubMenu, without terminating.
            // inout is used in several places, make it global
            int input = 0;
            String username = "";
            String password = "";
            // Main menu, make sure input is good!
            do {
                System.out.println("Main menu");
                System.out.println("1: Create user account");
                System.out.println("2: Login as user");
                System.out.println("3: List users");
                System.out.println("4: Exit");
                try {
                    System.out.println("Enter a choice (1-4)");
                    input = scanner.nextInt();
                } catch (InputMismatchException e) {
                    System.out.println("That is not a valid answer " + e);
                    break;
                }
                if (input < 1 || input > 4) {
                    System.out.println("That choice is outside the range of choice! ");
                } else {
                    break;
                }
            } while (true);
            // Depending on choice, do stuffs!
            switch (input) {
                // create user
                case 1:
                    bank.addUser();
                    Transaction create = new Transaction(bank.getUserByUsr(bank.tmpUserName).getAccount(), "create");
                    Bank.transactions.add(create);
                    break;
                // Log in
                case 2:
                    // Do stuff
                    System.out.println("Enter your username:");
                    username = scanner.next();
                    System.out.println("write your password");
                    password = scanner.next();
                    if(bank.findUserByUsernamePwd(username, password)){
                        if(bank.checkLogin(username, password)){
                            subMenu(bank.getUserFromUsrPwd(username, password));
                        }
                        else{
                            System.out.println("Wrong username/password");
                            break;
                        }
                    }
                    // If wrong username/password prints this
                    else{
                        System.out.println("Wrong username/password");
                    }
                    break;
                // List users
                case 3:
                    System.out.print(bank.toString());
                    break;
                //exit
                case 4:
                    System.out.println("Goodbye");
                    System.exit(0);
            }
        }
    }
    //submenu method
    public static void subMenu(User user){
        Bank bank = new Bank();
        Account account = user.getAccount();
        Scanner scan = new Scanner(System.in);
        while(true) {
            int subInput = 0;
            int amount;
            // Submenu, list of options
            do {
                System.out.println("SubMenu");
                System.out.println("1: Check balance");
                System.out.println("2: Deposit");
                System.out.println("3: Withdraw");
                System.out.println("4: List of transactions");
                System.out.println("5: Transfer to other account");
                System.out.println("6: Close user session");
                try {
                    System.out.println("Enter a choice (1-6)");
                    subInput = scan.nextInt();
                }
                catch (InputMismatchException e) {
                    System.out.println("That is not a valid answer");
                }
                if (subInput < 1 || subInput  > 6) {
                    System.out.println("That choice is outside the range of choice! ");
                }
                else {
                    break;
                }
            }
            while (true);
            // Depending on choice, do stuffs!
            switch (subInput)
            {
                //Check balance
                case 1:
                    System.out.println("Balance :" + account.getAmount());
                    int bal = account.getAmount();
                    // transaction
                    Transaction ga = new Transaction(account, "balance check " + bal);
                    Bank.transactions.add(ga);
                    break;
                //Deposit
                case 2:
                    System.out.println("Enter amount you want to deposit");
                    amount = scan.nextInt();
                    account.deposit(amount);

                    System.out.println(amount + " deposited");
                    // Record transaction
                    Transaction dp = new Transaction(account, "deposit " + amount);
                    Bank.transactions.add(dp);
                    break;
                //Withdraw
                case 3:
                    System.out.println("Enter amount you want to withdraw");
                    amount = scan.nextInt();
                    if (account.withdraw(amount)) {
                        Transaction wd = new Transaction(account, "withdraw " + amount);
                        Bank.transactions.add(wd);
                    }
                    else {
                        System.out.println("Withdrawal failed");
                    }
                    break;
                //List of transactions
                case 4:
                    for (Transaction o : Bank.transactions) {
                        if (o.getAccount() == user.getAccount()){
                            System.out.println(o.toString());
                        }
                    }
                    break;
                //Transfer to other account
                case 5:
                    System.out.println("How much do you want to transfer? ");
                    int trans = scan.nextInt();
                    System.out.println("To what account do you want to transfer to? ");
                    String to = scan.next();
                    // Test the transaction, returns true if transfer completes
                    if (account.transferMoney(bank.getUserByUsr(to), trans)) {
                        Transaction tm = new Transaction(account, "transfer " + trans + " to " + to);
                        Bank.transactions.add(tm);
                    }
                    else {
                        System.out.println("Transfer failed, not enough money! ");
                    }
                    break;
                //Close user session
                case 6:
                    MainMenu();
            }
        }
    }
}